var Pelis = require('../model/peli.js');
// no cal el modul formidable //
module.exports = function(app){
	// funcio que llista les pelicules que tenim a mongo //
	llistarPelis = function(req,res){
		// Pelicules es la col·leccio de documents 
		Pelis.find(function(err,Pelicules){
			// si no hi ha hagut cap error //
			if(!err){
				res.render('mostrarPelis.jade',{pelis: Pelicules});
			}else		
				console.log("Error " + err);	
		});
	}
	// funcio que filtra una pelicula per id //
	filtrarCodpeli = function(req,res){
		var _id = req.params.id;
		Pelis.find({_id:_id},function(err,result){
			if(!err){
				res.render('filtrarPeli.jade', {pelis: result});
			}else{
				res.render('filtrarPeli.jade',{error:"error, no tenim pelicula amb aquest codpeli"});
			}
		});
	}
	// funcio que borra una pelicula per id //
	borrarPeli = function(req,res){
		var _id = req.params.id;
		res.charset = 'utf8';
		Pelis.remove({_id:_id},function(err){
			if(err){
				// cas que no s'introdueixi un id valid
				res.render('borrarPeli.jade',{error:"No existeix la pelicula amb un id invalid"});
			}else{
				res.render('borrarPeli.jade',{peliBorrada:"Pelicula borrada correctament"});
			}
		});
	}
	// funcio que afegeix una pelicula al videoclub//
	afegirPeli = function(req,res){
		var _id = req.body._id;
		var titol = req.body.titol;
		var sinopsi = req.body.sinopsi;
		var novaPeli = new Pelis({
			_id: _id,
			titol: titol,
			sinopsi: sinopsi
		});
		novaPeli.save(function(err, novaPeli) {
  			if (err) return res.render('resultatPeli.jade',{error: "Error, hi ha una pelicula amb aquest codpeli"});
  				res.render('resultatPeli.jade',{ pelis:novaPeli, error: "Pelicula afegida correctament"});
		});
	}
	// funcio que actualitza una pelicula si existeix al videoclub //
	updatePeli = function(req,res){
		var _id = req.body._id;
		var titol = req.body.titol;
		var sinopsi = req.body.sinopsi;
		// TRACTAMENT DEL _ID //
		Pelis.find ({_id:_id},function(err,result){
			var missatge;
			if(!err){
				// CAS QUE NO TROBEM LA PELICULA //
				if (result.length == 0){
					missatge = "No hi ha cap pelicula amb aquest codpeli";
					// FUTURIBLE MILLORA: afegir directament //
				}else{
					//console.log("Longitut de result" + result.length);
					// modifiquem les dades, l'id sera el mateix //
					var novaPeli = new Pelis({
						titol: titol,
						sinopsi: sinopsi
					});
					var upsertData = novaPeli.toObject();
					// Dades actualitzades com vol l'usuari, no tindra id //
					Pelis.update({_id: result[0]._id}, upsertData, {upsert: true}, function(err){
						if (err){
							missatge="ERROR AL ACTUALITZAR";	
						}
					});
					missatge="PELICULA ACTUALITZADA CORRECTAMENT";
				}
			}else if(result == undefined){
				missatge = "ID no vàlid";
			}
			res.render('missatge.jade',{missatge: missatge});
		});
	}

	// endpoints //
	// endpoint per llistar totes les pelicules //
	app.get('/llistar',llistarPelis);
	// endpoint per llistar una pelicula en concret //
	app.get('/llistar/:id',filtrarCodpeli);
	// endpoint per quan es selecciona la pelicula a modificar //
	// endpoint que borra una pelicula per codpeli //
	app.get('/borrar/:id',borrarPeli);
	// endpoint que afegeix una pelicula //
	//insert new movie
	app.get('/afegir', function (req, res) {
		res.render('inserirPelis.jade');
	});
	app.post('/accioAfegir', afegirPeli);
	// endpoint get d'actualitzar
	app.get('/actualitzar',function(req,res){
		res.render('peliModificar.jade');
	});
	// ENDPOINT POST D'actualitzar
	app.post('/actualitzacio',updatePeli);
};