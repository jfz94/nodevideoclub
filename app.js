    var express = require('express');
    bodyParser = require('body-parser');
	mongoose = require('mongoose');
	app = express();

// Formulari //
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');


app.get('/', function(req,res){
	res.render('benvinguda.jade',{titol: "Benvingut al videoclub!"});
});

mongoose.connect('mongodb://localhost/videoclub', function(err, res){
	if(!err)
		console.log("Connectat correctament");
	else
		console.log("Error" + err);
});

// es pasa el control de l'app al controller.js
require('./routes/controller')(app);
// pelicules el nom de la BBDD
app.listen(3000, function(){
	console.log("Server running on port 3000, htpp://localhost:3000");
});
