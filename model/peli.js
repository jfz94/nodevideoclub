var mongoose = require('mongoose');
// et retorna un tipus d'objecte que es el que s'instancia //
var Schema = mongoose.Schema;
var schemaPeli = new Schema ({
	_id: Number,
	titol: String,
	sinopsi: String
});
// aqui es troba la col·leccio //
// definim el nom de la coleccio //
module.exports = mongoose.model('Pelicules', schemaPeli);